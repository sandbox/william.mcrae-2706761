-- SUMMARY --
Adding classes to Web Forms form elements can be a pain. This module adds
a class to the form element based on the webform title. Extra classes can
be added in the advanced settings.

-- CONFIGURATION --

Once enabled all webform form element will include a new class based on
the title of the webform.

Addition classes can be added to the form element by editting the each
webform, in the Advanced Settings the "Form element class" can be used
to add a space separated list of class names. 

In both the webform title and additional classes the names are pass
through drupal_html_class() to ensure the class names are valid.
